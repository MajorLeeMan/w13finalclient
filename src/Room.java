//Room object class
/*
goblinnum (number of goblins)
treasurenum (number of treasures/items)
exits (number of exits to be taken)
 */
public class Room {

    //starter objects found in room
    int goblinnum=0;
    int treasurenum=0;
    //int exits=1;
    //int entrance=1;

    //getters
    public int getGoblinnum() {return goblinnum;}
    public int getTreasurenum() {return treasurenum;}
    //public int getExits() {return exits;}
    //public int getEntrance() {return entrance;}

    //setters
    public void setGoblinnum(int goblinnum) {this.goblinnum = goblinnum;}
    public void setTreasurenum(int treasurenum) {this.treasurenum = treasurenum;}
    //public void setExits(int exits) {this.exits = exits;}
    //public void setEntrance(int entrance) {this.entrance = entrance;}

    //room creation, based on player difficulty

    //return?
}
