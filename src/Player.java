//Player object class
/*
Hero (that the player selected)
Life (health the player has)
Inventory (an array)
InventoryItem (within the array)
Difficulty (of the room)
s (score object)
t (thread object to track time)
 */

public class Player {

    //creating a new player
    String hero="Jimmy";
    int difficulty=0;
    int life = 10;
    String inventory[]={"Sword","Shield","Trinket","Armor","Fork"};
    Score s = new Score(); //create a score object for the player
    Timer t = new Timer(); //create the threading object

    //t.interrupt(); //interrupt the thread and accumulate score

    //setters
    public void setHero(String hero) {this.hero = hero; }
    public void setLife(int life) {this.life = life;}
    public void setInventory(String[] inventory) {this.inventory = inventory;}
    public void setInventoryItem(String item, int number) {this.inventory[number] = item;}
    public void setDifficulty(int difficulty) {this.difficulty = difficulty;}
    public void setS(Score s) {this.s = s;}
    public void setT(Timer t) {this.t = t;}

    //getters
    public String getHero() {return hero;}
    public int getLife() {return life;}
    public String[] getInventory() {return inventory;}
    public String getInventoryItem(int number) {return inventory[number];}
    public int getDifficulty() {return difficulty;}
    public Score getS() {return s;}
    public Timer getT() {return t;}
}
