/*
Completed: Integrated Threading (accurately counts seconds and ends correctly).
    Program is currently input fault tolerant.

To do list: Integrate the ability to list the Inventory as an array,
    sorted either by power level or alphabetically.
    Integrate the ability to encapsulate objects using JSON.
    Integrate the programs ability to connect to a server using HTTP.
*/
import java.io.IOException;
import java.util.Scanner;

public class Main extends Thread {

    public static void main(String[] args) { //this program will play a small game for the user to interact with

        System.out.format("%nStarting..."); //starting variables
        Scanner key = new Scanner(System.in);
        int i=0; //while loop counter
        char c; //c for character
        String s; //s for string
        int n; //n for number
        boolean flee = false;

        while (i==0) {

            //Ask the player if they would like to play a game or view high scores. Y/N/H
            System.out.format("%nWould you like to play a game?" +
                    "%n Yes / No / High scores (Y/N/H): ");
            c=key.next().charAt(0); //take the first character entered

            switch (c) {
                case 'y', 'Y' -> {
                    //time to play a game
                    System.out.format("%nLets play a game then. ");

                    //If yes, pick a hero and begin
                    key.nextLine();
                    System.out.format("%nWhat is your Hero's name?: ");
                    s = key.nextLine(); //obtain Hero name
                    System.out.format("%nWhat difficulty to start at? (1 for easy, 10 for hard): ");
                    n = key.nextInt();
                    System.out.format("%nYour hero steps up to the challenge... ");

                    //create player
                    Player p = new Player();
                    p.setHero(s);
                    p.setDifficulty(n);
                    flee = false;

                    //create room for the player
                    Room r = new Room();
                    r.setGoblinnum(p.getDifficulty());
                    r.setTreasurenum(p.getDifficulty()/2);

                    //begin timer
                    p.t.start();

                    i=1; //game loop
                    System.out.format("%nYou enter the room. ");
                    while (i==1) {
                        System.out.format("%nYou see %d goblins, but also %d treasure. ", r.getGoblinnum(), r.getTreasurenum());
                        System.out.format("You have %d health points remaining. It's been %d seconds so far.", p.getLife(), p.t.time);
                        System.out.format("%nEnter 1 to defeat a goblin, enter 2 to gather some treasure, enter 3 to flee with what you have.");
                        key.nextLine();//clear input
                        c=key.next().charAt(0);//based on what the player put in (1 2 or 3)
                        Decision(c, r, p, flee);//variables in the room might have changed or nothing happened
                        if (c=='3') {flee=true;}

                        //resolve what happens after the player makes a decision
                        if (flee==false) {//if the character didn't flee

                            if (r.getGoblinnum()<=0) {//check for goblins
                                //haha no goblins, do not notify the player
                            } else { //but if there ARE goblins
                                n = (r.getGoblinnum() / 2);
                                System.out.format("%nThe goblins attack for %d damage.", n);
                                p.setLife(p.getLife()-n); //damage the player
                            }

                            if (p.getLife() <= 0) {//if player health is 0 or below
                                System.out.format("%nYou have perished by goblins...");
                                i=2;
                            }
                        } else {//if flee==true
                            System.out.format("%nYou escape the room...");
                            i=2;//exit the room loop

                        }
                    }
                    //after either dying or fleeing, enumerate score
                    p.t.interrupt();
                    System.out.format("%nYou took %d seconds.", p.t.time);
                    System.out.format("%nYou scored %d points.", p.getS().getScore());
                    break;
                }

                case 'n', 'N' -> {
                    //time to exit
                    System.out.format("%nThank you, goodbye.");
                    i=2; //exit the loop
                    System.exit(0);
                    break;
                    }

                case 'h', 'H' -> {
                    i=3; //exit the loop
                    break;
                }

                default -> {
                    System.out.format("%nNot a valid response. Please enter Y, N, or H.");
                    i=0; //continue the loop
                }

            }
        }
        //if we got this far, we exited the loop
        System.out.format("%nThank you for playing. Reached the end of the code.");
    }

    public static void Decision(char c, Room r, Player p, boolean flee) {
        switch (c) {

            case '1': {//player chose to attack
                if (r.getGoblinnum()>0) {//if there are goblins
                    int n = (r.getGoblinnum() - 1);//defeat one
                    r.setGoblinnum(n);//set the new goblin value to the room
                    System.out.format("%nYou defeated a goblin");
                } else { //if there are no goblins
                    System.out.format ("%nThere are no goblins to attack.");
                }
                return;
            }

            case '2': {//player chose to loot
                if (r.getTreasurenum()>0) {//if there is loot
                    int n = (r.getTreasurenum() - 1);//take one treasure
                    p.getS().setScore(p.getS().getScore()+1000);//add to players score
                    r.setTreasurenum(n);//set the new treasure value
                    System.out.format("%nYou gather treasure and earn 1000 points.");
                } else {//if there is no treasure
                    System.out.format("%nThere is no more treasure here. You spend your turn looking for nothing.");
                }
                return;
            }

            case '3': {//player chose to flee
                System.out.format("%nYou run away...");
                flee = true;
                return;
            }

            default: {//did not enter 1 2 or 3
                System.out.format("%nEntered a wrong character. You miss your turn.");
                return;
            }

        }//end of switch c

    }//end of decision method

    //public static Enumerate() {

    //}

    public static void HitEnter(){
        //read the value (which won't be used) when enter is hit, which continues the code
        try {int read = System.in.read(new byte[2]);} catch (IOException i) {};
    }

}
