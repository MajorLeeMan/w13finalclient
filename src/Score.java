//high Score class
public class Score {

    String name = "";
    int time=0;
    int score=0;

    //getters
    public String getName() {return name;}
    public int getScore() {return score;}
    public int getTime() {return time;}

    //setters
    public void setName(String name) {this.name = name;}
    public void setScore(int score) {this.score = score;}
    public void setTime(int time) {this.time = time;}
}
