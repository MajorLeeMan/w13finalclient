//threading Timer class
public class Timer extends Thread {

    //object variables for the timer
    boolean counting=true;
    int time=0;

    public void run() {
        //create a new thread
        time=0;
        //System.out.format("%nDebug: The thread has begun.");
        while (counting) {
            time+=1;

            //System.out.println("Debug: time="+time);
            //This sleep cycle makes the thread count in seconds
            try {sleep(1000);} catch (InterruptedException a) {
                counting=false;
            }

            //set counting to false eventually
            //if (time>=5000) {counting=false;}

        }
        //System.out.format("%nDebug: End of run() method: "+time);
    }
}
